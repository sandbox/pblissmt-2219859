<?php

function drangular_admin_page() {
  return drupal_get_form("drangular_admin_form");
}

function drangular_admin_form($form, $form_state) {
      
  $form = array();
  
  $form['apps_table'] = array (
    '#type' => 'fieldset',        
    '#theme' => 'drangular_admin_table',
    '#tree' => TRUE,
  );
  
  $apps = drangular_get_apps("*");
  
  foreach($apps as $app) {
        
    $form['apps_table']['app_name'][$app->app_name] = array(                           
      '#markup' => $app->app_name
    );
    
    $form['apps_table']['app_folder'][$app->app_name] = array(
      '#markup' => $app->app_folder
    );
    
    $form['apps_table']['scripts_folder'][$app->app_name] = array(
      '#markup' => $app->scripts_folder
    );
    
    $form['apps_table']['styles_folder'][$app->app_name] = array(
      '#markup' => $app->styles_folder
    );  
      
    $path_data = "<br>";
    
    //simply for asthetics
    $numItems = count($app->paths);
    $i = 0;

    foreach($app->paths as $path) {
     
      $path_data .= "<b>Path:</b> {$path->path}<br>";
      
      if($path->create_link == 1) {
        $path_data .= "<b>Menu Title:</b> {$path->title}<br>";
        $path_data .= "<b>Menu Description:</b> {$path->description}<br>";
        $path_data .= "<b>Template:</b> {$path->template}<br>";
      }
      
      $path_data .= "<br><a href='/admin/config/development/drangular/edit/{$app->app_name}/paths/edit/{$path->path}'>Edit Path</a>";
      $path_data .= "<br><a href='/admin/config/development/drangular/edit/{$app->app_name}/paths/delete/{$path->path}'>Delete Path</a>";
      
      if(++$i !== $numItems) {
        $path_data .= "<br><hr><br>";
      }
    }
    
    $form['apps_table']['paths'][$app->app_name] = array(
      '#markup' => $path_data
    );  
    
    $form['apps_table']['actions'][$app->app_name]['edit'] = array(
      '#markup' => "<a href='/admin/config/development/drangular/edit/{$app->app_name}'>Edit App</a><br>"
    );
    
    $form['apps_table']['actions'][$app->app_name]['delete'] = array(
      '#markup' => "<a href='/admin/config/development/drangular/delete/{$app->app_name}'>Delete App</a><br>"
    ); 
      
    $form['apps_table']['actions'][$app->app_name]['edit-cache'] = array(
      '#markup' => "<a href='/admin/config/development/drangular/edit-cache/{$app->app_name}'>View / Edit Cache</a><br>"
    ); 
      
    $form['apps_table']['actions'][$app->app_name]['clear'] = array(
      '#markup' => "<a href='/admin/config/development/drangular/clear/{$app->app_name}'>Clear Cache</a><br>"
    ); 
      
    $form['apps_table']['actions'][$app->app_name]['edit_paths'] = array(
      '#markup' => "<a href='/admin/config/development/drangular/edit/{$app->app_name}/paths/add'>Add Path</a><br>"
    );
  }
  
  $form['add_app'] = array(
    '#markup' => "<a href='/admin/config/development/drangular/add'>Add new App</a>"
  );
      
  return $form;
  
}


function drangular_admin_form_submit($form, $form_state) {
  
}

function theme_drangular_admin_table($variables) {
  
  $form = $variables['form'];
    
  $header = array(t('App Name'), t('App Folder'), t('Scripts Folder'), t('Styles Folder'), t('Paths'), t('Actions'));
  
  $rows = array();
     
  foreach (element_children($form['app_name']) as $key) {
      
    $record = array();
    $record[] = drupal_render($form['app_name'][$key]);
    $record[] = drupal_render($form['app_folder'][$key]);
    $record[] = drupal_render($form['scripts_folder'][$key]);
    $record[] = drupal_render($form['styles_folder'][$key]);
    $record[] = drupal_render($form['paths'][$key]);
    $record[] = drupal_render($form['actions'][$key]);
    $rows[] = $record;
  }
    
  $output = theme('table', array(
      'header' => $header,
      'rows' => $rows
  ));

  return $output;
}

function theme_drangular_cache_table($variables) {
  $form = $variables['form'];
    
  $header = array(t('Script'));
  
  $rows = array();
     
  foreach (element_children($form['script_table']) as $key) {
      
    $record = array();
    $record[] = drupal_render($form[$key]);    
    $rows[] = $record;
  }
    
  $output = theme('table', array(
      'header' => $header,
      'rows' => $rows
  ));

  return $output;
}

function drangular_add_page() {
  return drupal_get_form("drangular_admin_edit_form");
}

function drangular_edit_page($app_name) {
  return drupal_get_form("drangular_admin_edit_form", $app_name);
}

function drangular_admin_edit_form($form, $form_state, $app_name = null) {
    
  if($app_name != null) {  
    $app = drangular_get_apps("*", $app_name);
    $app = $app[0];
  }
  
  //App name
  $form['app_name'] = array(
    '#title' => t('App Name'),
    '#type' => 'textfield',      
    '#description' => t("Name of the angular app as its defined in the app."),
    '#default_value' => (isset($app->app_name) ? $app->app_name : ""),
    '#required' => true,    
    '#size' => 50,
  );  
  
  //app folder
  $form['app_folder'] = array(
    '#title' => t('App Folder'),
    '#type' => 'textfield',      
    '#description' => t("Folder that the angular app lives in. This is in relation to the drangular module."),
    '#default_value' => (isset($app->app_folder) ? $app->app_folder : ""),
    '#required' => true,    
    '#size' => 50,
  );
  
  //script folder
  $form['scripts_folder'] = array(
    '#title' => t('Script Folder'),
    '#type' => 'textfield',      
    '#description' => t("The folder inside the app folder that contains the script files."),
    '#default_value' => (isset($app->scripts_folder) ? $app->scripts_folder : ""),
    '#required' => true,    
    '#size' => 50,
  );
  
  //styles folder
  $form['styles_folder'] = array(
    '#title' => t('Styles Folder'),
    '#type' => 'textfield',      
    '#description' => t("The folder inside the app folder that contains the style files."),
    '#default_value' => (isset($app->styles_folder) ? $app->styles_folder : ""),
    '#required' => true,    
    '#size' => 50,
  );
  
  //Cache ttl
  $form['cache_length'] = array(
    '#title' => t('Cache Length'),
    '#type' => 'textfield',      
    '#description' => t("The time to live of the script and style cache. Lower limit will cause the module to scan for new files more often."),
    '#default_value' => (isset($app->cache_length) ? $app->cache_length : "9999"),
    '#required' => true,    
    '#size' => 10,
  );
    
 $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'), 
    '#weight' => 99,
  );
  
  return $form;
}

function drangular_admin_edit_form_submit($form, $form_state) {
 
  
  $app_name = isset($form_state['build_info']['args'][0]) ? $form_state['build_info']['args'][0] : false;
    
  if($app_name) {
    db_update("drangular_apps")
      ->condition("app_name", $app_name)      
      ->fields(array("app_name" => $form_state['values']['app_name'],
          "app_folder" => $form_state['values']['app_folder'],
          "scripts_folder" => $form_state['values']['scripts_folder'],
          "styles_cache" => "",
          "styles_cache_date" => "0",
          "scripts_cache" => "",
          "scripts_cache_date" => "0",
          "cache_length" => $form_state['values']['cache_length'],
          "styles_folder" => $form_state['values']['styles_folder']))
      ->execute();
                
    drupal_set_message("App Updated");  
  }
  else {
    db_insert("drangular_apps")
      ->fields(array("app_name" => $form_state['values']['app_name'],
          "app_folder" => $form_state['values']['app_folder'],
          "scripts_folder" => $form_state['values']['scripts_folder'],
          "styles_cache" => "",
          "styles_cache_date" => "0",
          "scripts_cache" => "",
          "scripts_cache_date" => "0",
          "cache_length" => $form_state['values']['cache_length'],
          "styles_folder" => $form_state['values']['styles_folder']))
      ->execute();
    
    //add the default path    
    db_insert("drangular_apps_paths")
      ->fields(array("path" => "*",
          "title" => "",
          "description" => "",
          "template" => "default",
          "create_link" => "0",
          "app_name" => $form_state['values']['app_name']))
      ->execute();
  }
    
  drupal_goto("admin/config/development/drangular");
}

function drangular_edit_paths_add($app_name) {
  return drupal_get_form("drangular_edit_path_form", $app_name);
}

function drangular_edit_path($app_name, $path) {
  return drupal_get_form("drangular_edit_path_form", $app_name, $path);
}

function drangular_edit_path_form($form, $form_state, $app_name, $path = null) {
  
  $path_data = array();
  
  if($path != null) {
    $query = db_select("drangular_apps_paths", "dp")
      ->fields("dp");      
    $query->condition("app_name", $app_name);
    $query->condition("path", $path);
    $path_data = $query->execute()->fetchAssoc();           
  }
  
  $form = array();
  
  $form['path'] = array(
    '#title' => t('Path'),
    '#type' => 'textfield',      
    '#description' => t("Path the app will be loaded on."),
    '#default_value' => (array_key_exists("path", $path_data) ? $path_data['path'] : ""),
    '#required' => true,    
    '#size' => 50,
  );  
  
  $form['title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',      
    '#description' => t("If using a template, this is the menu title of the page."),
    '#default_value' => (array_key_exists("title", $path_data) ? $path_data['title'] : ""),
    '#required' => FALSE,    
    '#size' => 50,
  );
    
  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textfield',      
    '#description' => t("If using a template, the is the description of the page."),
    '#default_value' => (array_key_exists("description", $path_data) ? $path_data['description'] : ""),
    '#required' => FALSE,    
    '#size' => 50,
  );
  
  //styles folder
  $form['template'] = array(
    '#title' => t('Template'),
    '#type' => 'textfield',      
    '#description' => t("Template file name used. If you are not using a custom one 'default' is required."),
    '#default_value' => (array_key_exists("template", $path_data) ? $path_data['template'] : "default"),
    '#required' => true,    
    '#size' => 50,
  );    
    
 $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'), 
    '#weight' => 99,
  );
 
 return $form;
}

function drangular_edit_path_form_submit($form, $form_state) {
  $app_name = $form_state['build_info']['args'][0];
  $path = (isset($form_state['build_info']['args'][1]) ? $form_state['build_info']['args'][0] : false);
  
  if($path) {
    db_update("drangular_apps_paths")
      ->condition("app_name", $app_name)
      ->condition("path", $path)
      ->fields(array("path" => $form_state['values']['path'],
          "title" => $form_state['values']['title'],
          "description" => $form_state['values']['description'],
          "template" => $form_state['values']['template']))
      ->execute();
                
    drupal_set_message("Path Updated");  
  }
  else {
    $valid = drupal_valid_path($form_state['values']['path']);
    
    $x = (int)$valid;
    
    db_insert("drangular_apps_paths")
      ->fields(array("path" => $form_state['values']['path'],
          "app_name" => $app_name,
          "title" => $form_state['values']['title'],
          "description" => $form_state['values']['description'],
          "create_link" => (int)!$valid,
          "template" => $form_state['values']['template']))
      ->execute();
    
    drupal_set_message("Path Added");  
  }
    
  //force a cache clear to enable the menu link  
  cache_clear_all();  
  menu_rebuild();
    
  drupal_goto("admin/config/development/drangular");
}

function drangular_delete_app($app_name) {
 
  db_delete("drangular_apps")
    ->condition("app_name", $app_name)
    ->execute();
  
  drupal_set_message("App Deleted");  
  drupal_goto("admin/config/development/drangular");
}

function drangular_delete_path($app_name, $path) {
  
  db_delete("drangular_apps_paths")
    ->condition("app_name", $app_name)
    ->condition("path", $path)
    ->execute();
  
  drupal_set_message("Path Deleted");  
  drupal_goto("admin/config/development/drangular");
}

function drangular_edit_cache_page($app_name) {
        
  $form = array();
    
  $app = drangular_get_apps("*", $app_name);
    
  $app = $app[0];
  
  //option to never expire cache
  $form['never_expire'] = array(
    '#type' => 'checkbox', 
    '#title' => t('Cache Never Expire'),
    '#default_value' => "",
  );
  
  //clear cache
  $form['clear_scripts'] = array(
    '#markup' => "<a href='/admin/config/development/drangular/clear/{$app_name}/Script'>Clear script cache</a><br>"
  );
  
  //clear cache
  $form['clear_styles'] = array(
    '#markup' => "<a href='/admin/config/development/drangular/clear/{$app_name}/Style'>Clear style cache</a>"
  );
      
  //table of current cache
  $form['script_table'] = array (
    '#title' => 'Scripts Loaded',
    '#type' => 'fieldset',        
    '#theme' => 'drangular_cache_table',
    '#tree' => TRUE,
  );
  
  $scripts = unserialize($app->scripts_cache);
  $styles = unserialize($app->styles_cache);
  
  foreach($scripts as $script) {
    $form['script_table'][$script] = array(
      'name' => array(
            '#markup' => check_plain($script),
          ),
      'weight' => array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#default_value' => 1,
        '#delta' => 10,
        '#title_display' => 'invisible',
      ),
    );
  }
  
  $form['add_script_fieldset'] = array (
    '#title' => 'Add script to cache',
    '#type' => 'fieldset',            
    '#tree' => TRUE,
  );

  //add file to cache
  $form['add_script_fieldset']['script_file'] = array(
    '#title' => t('Script File Path'),
    '#type' => 'textfield',      
    '#description' => t("Full path to the file to include relative to the app folder"),
    '#default_value' => "",
    '#required' => true,    
    '#size' => 50,
  );
  
  $form['add_script_fieldset']['add_script'] = array(
    '#type' => 'submit',
    '#value' => t('Add Script'), 
    '#weight' => 99,
  );    
  
  $form['style_table'] = array (
    '#title' => 'Styles Loaded',
    '#type' => 'fieldset',        
    '#theme' => 'drangular_cache_table',
    '#tree' => TRUE,
  );  
  
  //add file to cache
  $form['add_style_fieldset'] = array (    
    '#title' => 'Add style to cache',
    '#type' => 'fieldset',            
    '#tree' => TRUE,
  );

  //add file to cache
  $form['add_style_fieldset']['style_file'] = array(
    '#title' => t('Style File Path'),
    '#type' => 'textfield',      
    '#description' => t("Full path to the file to include relative to the app folder"),
    '#default_value' => "",
    '#required' => true,    
    '#size' => 50,
  );
  
  $form['add_style_fieldset']['add_style'] = array(
    '#type' => 'submit',
    '#value' => t('Add Style'), 
    '#weight' => 99,
  );      
  
  return $form;
}